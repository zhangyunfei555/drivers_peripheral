# Copyright (c) 2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")
import("//drivers/peripheral/distributed_camera/distributedcamera.gni")

ohos_shared_library("libdistributed_camera_provider_config") {
  include_dirs =
      [ "${distributedcamera_hdf_path}/hdi_service/include/dcamera_provider" ]
  sources = [ "./src/config/dcamera_provider_config.cpp" ]
  deps = [ "${distributedcamera_hdf_path}/hdi_service:libdistributed_camera_hdf_service_1.0" ]

  external_deps = [
    "hdf_core:libhdf_host",
    "hdf_core:libhdf_ipc_adapter",
    "hdf_core:libhdf_utils",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
    "utils_base:utils",
  ]

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_distributed_camera"
}

ohos_shared_library("libdistributed_camera_host_config") {
  include_dirs = [
    "${distributedcamera_hdf_path}/interfaces/hdi_ipc/server/host",
    "${distributedcamera_hdf_path}/interfaces/hdi_ipc/server/device",
    "${distributedcamera_hdf_path}/interfaces/hdi_ipc/server/operator",
    "${distributedcamera_hdf_path}/hdi_service/include/dcamera_host",
    "${distributedcamera_hdf_path}/hdi_service/include/dcamera_device",
    "${distributedcamera_hdf_path}/hdi_service/include/dstream_operator",
    "${distributedcamera_hdf_path}/hdi_service/include/utils",
    "//utils/native/base/include",
    "//utils/system/safwk/native/include",
    "${camera_hdf_path}/camera/interfaces/include",
    "${camera_hdf_path}/camera/interfaces/hdi_ipc",
    "${hdf_framework_path}/include/utils",
    "${hdf_framework_path}/include/core",
    "${hdf_framework_path}/include/osal",
    "${hdf_uhdf_path}/include/hdi",
    "${hdf_uhdf_path}/osal/include",
    "${hdf_uhdf_path}/ipc/include",
    "${hdf_uhdf_path}/include/host",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "//third_party/jsoncpp/include",
  ]

  sources = [ "./src/config/dcamera_host_config.cpp" ]

  deps = [
    "${distributedcamera_hdf_path}/hdi_service:libdistributed_camera_hdf_service_1.0",
    "//utils/native/base:utils",
  ]

  public_deps = [ "//drivers/interface/distributed_camera/v1_0:libdistributed_camera_provider_stub_1.0" ]

  defines = [
    "HI_LOG_ENABLE",
    "DH_LOG_TAG=\"distributedcamerahdf\"",
    "LOG_DOMAIN=0xD004100",
  ]

  external_deps = [
    "graphic_chipsetsdk:buffer_handle",
    "graphic_chipsetsdk:surface",
    "hdf_core:libhdf_host",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
  ]

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_distributed_camera"
}

ohos_shared_library("libdistributed_camera_hdf_service_1.0") {
  include_dirs = [
    "include/dcamera_device",
    "include/dcamera_host",
    "include/dcamera_provider",
    "include/dstream_operator",
    "include/utils",
    "../interfaces/hdi_ipc",
    "../interfaces/hdi_ipc/server/device",
    "../interfaces/hdi_ipc/server/host",
    "../interfaces/hdi_ipc/server/operator",
    "//utils/native/base/include",
    "//utils/system/safwk/native/include",
    "${display_hdf_path}/interfaces/include",
    "${hdf_framework_path}/include/utils",
    "${hdf_framework_path}/include/core",
    "${hdf_framework_path}/include/osal",
    "${hdf_uhdf_path}/include/hdi",
    "${hdf_uhdf_path}/osal/include",
    "${hdf_uhdf_path}/ipc/include",
    "${hdf_uhdf_path}/include/host",
    "//third_party/jsoncpp/include",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "${camera_hdf_path}/camera/interfaces/include",
    "${camera_hdf_path}/camera/interfaces/hdi_ipc",
  ]

  sources = [
    "../interfaces/hdi_ipc/server/device/dcamera_device_callback_proxy.cpp",
    "../interfaces/hdi_ipc/server/device/dcamera_device_stub.cpp",
    "../interfaces/hdi_ipc/server/host/dcamera_host_callback_proxy.cpp",
    "../interfaces/hdi_ipc/server/host/dcamera_host_stub.cpp",
    "../interfaces/hdi_ipc/server/operator/doffline_stream_operator_stub.cpp",
    "../interfaces/hdi_ipc/server/operator/dstream_operator_callback_proxy.cpp",
    "../interfaces/hdi_ipc/server/operator/dstream_operator_stub.cpp",
    "src/dcamera_device/dcamera_device.cpp",
    "src/dcamera_device/dmetadata_processor.cpp",
    "src/dcamera_host/dcamera_host.cpp",
    "src/dcamera_provider/dcamera_provider.cpp",
    "src/dstream_operator/dbuffer_manager.cpp",
    "src/dstream_operator/dcamera_stream.cpp",
    "src/dstream_operator/dimage_buffer.cpp",
    "src/dstream_operator/doffline_stream_operator.cpp",
    "src/dstream_operator/dstream_operator.cpp",
    "src/utils/anonymous_string.cpp",
    "src/utils/dcamera.cpp",
    "src/utils/dh_log.cpp",
  ]

  public_deps = [ "//drivers/interface/distributed_camera/v1_0:libdistributed_camera_provider_stub_1.0" ]

  deps = [
    "//drivers/hdf_core/adapter/uhdf2/ipc:libhdf_ipc_adapter",
    "//drivers/peripheral/camera/interfaces/metadata:metadata",
    "//third_party/jsoncpp:jsoncpp",
    "//utils/native/base:utils",
  ]

  defines = [
    "HI_LOG_ENABLE",
    "DH_LOG_TAG=\"distributedcamerahdf\"",
    "LOG_DOMAIN=0xD004100",
  ]

  external_deps = [
    "graphic_chipsetsdk:buffer_handle",
    "graphic_chipsetsdk:surface",
    "hdf_core:libhdf_utils",
    "hiviewdfx_hilog_native:libhilog",
    "ipc:ipc_single",
    "utils_base:utils",
  ]

  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_distributed_camera"
}

group("hdf_distributed_camera_service") {
  deps = [
    ":libdistributed_camera_hdf_service_1.0",
    ":libdistributed_camera_host_config",
    ":libdistributed_camera_provider_config",
  ]
}
